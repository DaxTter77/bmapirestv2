package com.bmsoftware.apirest.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import org.apache.log4j.Logger;
import java.util.ArrayList;

import java.util.List;

import com.bmsoftware.apirest.entities.PersonaDTO;
import com.bmsoftware.apirest.entities.TliqcaParametrosDTO;
import com.bmsoftware.apirest.entities.base.ForaneaDTO;
import com.bmsoftware.apirest.exception.AppException;
import com.bmsoftware.apirest.interfaces.persistence.ITliqcaParametrosDAO;
import com.bmsoftware.apirest.interfaces.services.ITliqcaParametrosService;
import com.bmsoftware.apirest.interfaces.services.base.IAppExceptionService;
import com.bmsoftware.apirest.utils.Parameters;

@Repository
public class TliqcaParametrosDAO extends GenericDAO implements ITliqcaParametrosDAO {
	private static final Logger bitacora = Logger.getRootLogger();

	@Autowired
	private IAppExceptionService exceptionService;

	public Long create(TliqcaParametrosDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");

		try {
			sql.append("INSERT INTO TLIQCA_PARAMETROS (" + " ID_PARAM," + " NOMBRE," + " DESCRIPCION," + " VALOR" + " )"
					+ " VALUES (" + " :idParam," + " :nombre," + " :descripcion," + " :valor" + " )");
			String colum[] = new String[1];
			colum[0] = "ID_PARAM";

			return super.create(entity, sql.toString(), colum);
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosDAO.create. Causa: " + e.getMessage() + ". Query: " + sql.toString(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	public void update(TliqcaParametrosDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("UPDATE TLIQCA_PARAMETROS SET " + " NOMBRE=:nombre," + " DESCRIPCION=:descripcion,"
					+ " VALOR=:valor" + " WHERE " + "ID_PARAM=:idParam");

			super.update(entity, sql.toString());
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosDAO.update. Causa: " + e.getMessage() + ". Query: " + sql.toString(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}

	}

	public void delete(TliqcaParametrosDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("DELETE FROM TLIQCA_PARAMETROS " + " WHERE " + "ID_PARAM=:idParam");
			super.delete(entity, sql.toString());
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosDAO.delete. Causa: " + e.getMessage() + ". Query: " + sql.toString(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	@Override
	public List<TliqcaParametrosDTO> findByCriteria(TliqcaParametrosDTO entity, String orderField, boolean isDesc)
			throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("select" + " ID_PARAM," + " NOMBRE," + " DESCRIPCION," + " VALOR" + " FROM TLIQCA_PARAMETROS "
					+ " WHERE 1=1 ");
			if (entity.getIdParam() != null && entity.getIdParam().intValue() != 0)
				sql.append(" AND ID_PARAM=:idParam");
			if (entity.getNombre() != null && !entity.getNombre().equals(""))
				sql.append(" AND upper(NOMBRE) like  '%'||upper('%" + entity.getNombre() + "%')||'%'");
			if (entity.getDescripcion() != null && !entity.getDescripcion().equals(""))
				sql.append(" AND upper(DESCRIPCION) like  '%'||upper('%" + entity.getDescripcion() + "%')||'%'");
			if (entity.getValor() != null && !entity.getValor().equals(""))
				sql.append(" AND upper(VALOR) like  '%'||upper('%" + entity.getValor() + "%')||'%'");

			return super.findByCriteria(entity, sql.toString(), orderField, isDesc);
		} catch (Exception e) {
			bitacora.error(
					"TliqcaParametrosDAO.findByCriteria. Causa: " + e.getMessage() + ". Query: " + sql.toString(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}
	
	@Override
	public List<PersonaDTO> findPersona(PersonaDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("select" + " * " + " FROM PERSONA ");

			return super.findByCriteria(entity, sql.toString(), null, false);

		} catch (Exception e) {
			bitacora.error(
					"TliqcaParametrosDAO.findByCriteria. Causa: " + e.getMessage() + ". Query: " + sql.toString(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	@Override
	public TliqcaParametrosDTO findByPK(TliqcaParametrosDTO entity) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {
			sql.append("select" + " ID_PARAM," + " NOMBRE," + " DESCRIPCION," + " VALOR" + " FROM TLIQCA_PARAMETROS "

					+ " WHERE " + "ID_PARAM=:idParam");

			return (TliqcaParametrosDTO) super.findByPK(entity, sql.toString());

		} catch (Exception e) {
			bitacora.error(
					"TliqcaParametrosDAO.findByCriteria. Causa: " + e.getMessage() + ". Query: " + sql.toString(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	@Override
	public List<TliqcaParametrosDTO> findByCriteriaForeign(TliqcaParametrosDTO entity) throws AppException {
		StringBuilder select = new StringBuilder("");
		StringBuilder where = new StringBuilder("");
		List<ForaneaDTO> listForaneaDTO = new ArrayList<>();
		ForaneaDTO objForaneaDTO = null;
		try {
			select.append("" + " ID_PARAM," + " NOMBRE," + " DESCRIPCION," + " VALOR");

			where.append(" WHERE 1=1 ");
			if (entity.getIdParam() != null)
				where.append(" AND TLIQCA_PARAMETROS.ID_PARAM=:idParam");

			if (entity.getNombre() != null && !entity.getNombre().equals(""))
				where.append(
						" AND upper(TLIQCA_PARAMETROS.NOMBRE) like  '%'||upper('%" + entity.getNombre() + "%')||'%'");

			if (entity.getDescripcion() != null && !entity.getDescripcion().equals(""))
				where.append(" AND upper(TLIQCA_PARAMETROS.DESCRIPCION) like  '%'||upper('%" + entity.getDescripcion()
						+ "%')||'%'");

			if (entity.getValor() != null && !entity.getValor().equals(""))
				where.append(
						" AND upper(TLIQCA_PARAMETROS.VALOR) like  '%'||upper('%" + entity.getValor() + "%')||'%'");

			return super.findByCriteriaForeign(entity, "TLIQCA_PARAMETROS", select.toString(), where.toString(),
					listForaneaDTO, entity.getAliasForaneas());

		} catch (Exception e) {
			bitacora.error("TliqcaParametrosDAO.findByCriteriaForeing. Causa: " + e.getMessage() + ". Query: "
					+ select.toString(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	@Override
	public List<TliqcaParametrosDTO> consultarNombre(TliqcaParametrosDTO entity, boolean creating) throws AppException {
		StringBuilder sql = new StringBuilder("");
		try {

			sql.append("select 1 FROM TLIQCA_PARAMETROS 	where nombre=:nombre");
			sql.append(creating ? "" : " and ID_PARAM!=:idParam ");

			return super.findByCriteria(entity, sql.toString(), null, false);
		} catch (Exception e) {
			bitacora.error(
					"TliqcaParametrosDAO.findByCriteria. Causa: " + e.getMessage() + ". Query: " + sql.toString(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

}
