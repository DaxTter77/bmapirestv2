package com.bmsoftware.apirest.persistence;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.bmsoftware.apirest.entities.base.ForaneaDTO;
import com.bmsoftware.apirest.exception.AppException;
import com.bmsoftware.apirest.interfaces.persistence.IGenericDAO;
import com.bmsoftware.apirest.interfaces.persistence.OracleJdbcTemplate;
import com.bmsoftware.apirest.interfaces.services.base.IAppExceptionService;
import com.bmsoftware.apirest.utils.Parameters;

@Component("GenericDAO")
public class GenericDAO extends NamedParameterJdbcDaoSupport implements IGenericDAO {
	public static final Logger bitacora = Logger.getRootLogger();

	@Autowired
	IAppExceptionService bmBaseExcepcionService;
	private OracleJdbcTemplate oracleTemplate;

	@Autowired
	@Qualifier("principalDataSource")
	public void dataSource(DataSource dataSource) {
		this.setDataSource(dataSource);
		oracleTemplate = new OracleJdbcTemplate(dataSource);
	}

	public void create(Object entity, String sql) throws AppException {
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
			this.getNamedParameterJdbcTemplate().update(sql, namedParameters);
		} catch (Exception e) {
			bitacora.error("GenericDAO.create. Causa: " + e.getMessage() + ". Query: " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	public Long create(Object entity, String sql, String[] columnNames) throws AppException {
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
			KeyHolder keyHolder = new GeneratedKeyHolder();

			this.oracleTemplate.update(sql, namedParameters, keyHolder, columnNames);
			return keyHolder.getKey() != null ? keyHolder.getKey().longValue() : null;
		} catch (Exception e) {
			bitacora.error("GenericDAO.create. Causa: " + e.getMessage() + ". Query: " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	public void update(Object entity, String sql) throws AppException {
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
			this.getNamedParameterJdbcTemplate().update(sql, namedParameters);
		} catch (Exception e) {
			bitacora.error("GenericDAO.update. Causa:" + e.getMessage() + ". Query: " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	public void delete(Object entity, String sql) throws AppException {
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
			this.getNamedParameterJdbcTemplate().update(sql, namedParameters);
		} catch (Exception e) {
			bitacora.error("GenericDAO.delete. Causa:" + e.getMessage() + ". Query: " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	public List findByCriteria(Object entity, String sql, String order, boolean isDesc) throws AppException {
		List results = null;
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
			if (order != null && !order.equals("")) {
				sql += " order by " + order + (isDesc ? " desc " : " asc ");
			}
			results = this.getNamedParameterJdbcTemplate().query(sql, namedParameters,
					new BeanPropertyRowMapper(entity.getClass()));
		} catch (Exception e) {
			bitacora.error("GenericDAO.findByCriteria. Causa: " + e.getMessage() + ". Query " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
		return results;
	}

	public Object findByPK(Object entity, String sql) throws AppException {
		List results = null;
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);

			results = this.getNamedParameterJdbcTemplate().query(sql, namedParameters,
					new BeanPropertyRowMapper(entity.getClass()));
		} catch (Exception e) {
			bitacora.error("GenericDAO.findByCriteria. Causa: " + e.getMessage() + ". Query " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
		if (results != null && !results.isEmpty()) {
			return results.get(0);
		}
		return null;
	}

	public void registerLog(String usuario, String emplid, String accion, String texto) throws AppException {
		Map<String, Object> parameters = null;
		String sql = null;
		try {
			sql = "INSERT INTO PUJ_SYSADM.LOG_USUAPP (FECHA,USUARIO,EMPLID,APLICACION,ACCION,TEXTO) "
					+ "VALUES (:fecha, :usuario, :emplid, :aplicacion, :accion, :texto)";

			parameters = new HashMap<>();
			parameters.put("fecha", new Timestamp(System.currentTimeMillis()));
			parameters.put("usuario", usuario);
			parameters.put("emplid", emplid);
			parameters.put("aplicacion", Parameters.NOMBRE_APLICACION);
			parameters.put("accion", accion);
			if (texto == null) {
				parameters.put("texto", "");
			} else {
				parameters.put("texto", texto);
			}

			this.getNamedParameterJdbcTemplate().update(sql, parameters);

		} catch (Exception e) {
			bitacora.error("GenericDAO.registerLog. Causa: " + e.getMessage() + ". Query: " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
	}

	public List findByCriteriaForeign(Object entity, String tabla, String select, String where,
			List<ForaneaDTO> foraneasList, HashMap<String, String> aliasForaneas) throws AppException {
		List results = null;
		String sql = null;
		StringBuilder selectStatement = new StringBuilder("select ");
		StringBuilder fromStatement = new StringBuilder("from " + tabla);
		String[] arrayCampos;
		String aliasCampoForeing = null;
		try {
			// se agregua el nombre de la tabla padre a sus respectivos campos
			arrayCampos = select.split(",");
			for (int i = 0; i < arrayCampos.length; i++) {
				selectStatement.append(tabla + "." + arrayCampos[i]);
				selectStatement.append(i != (arrayCampos.length - 1) ? "," : "");// se
																					// determina
																					// si
																					// es
																					// el
																					// ultimo
																					// campo
																					// para
																					// ponerle
																					// una
																					// coma
			}
			// Ajustamos el select con los campos que se requieren de las
			// foraneas
			for (int j = 0; j < foraneasList.size(); j++) {
				arrayCampos = foraneasList.get(j).getCampos().split(",");
				selectStatement.append(",");
				for (int i = 0; i < arrayCampos.length; i++) {
					// Si la longitud del campo es mayor a 27 no se le asigna el
					// prefijo
					if (arrayCampos[i].length() <= 27) {
						aliasCampoForeing = aliasForaneas.get(foraneasList.get(j).getTabla()) + "_" + arrayCampos[i];
					} else {
						aliasCampoForeing = arrayCampos[i];
					}

					selectStatement
							.append(foraneasList.get(j).getTabla() + "." + arrayCampos[i] + " " + aliasCampoForeing);// se
																														// pone
																														// en
																														// el
																														// select
																														// los
																														// campos
																														// de
																														// la
																														// foranea
																														// y
																														// su
																														// respectivo
																														// aleas
					selectStatement.append(i != (arrayCampos.length - 1) ? "," : "");// se
																						// determina
																						// si
																						// es
																						// el
																						// ultimo
																						// campo
																						// para
																						// ponerle
																						// una
																						// coma
				}
				// creamos el join
				fromStatement.append(" JOIN " + foraneasList.get(j).getTabla() + " ON " + tabla + "."
						+ foraneasList.get(j).getCampoForanea() + "=" + foraneasList.get(j).getTabla() + "."
						+ foraneasList.get(j).getIdForanea());
			}

			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
			sql = selectStatement.toString() + " " + fromStatement.toString() + " " + (where == null ? "1=1" : where);
			results = this.getNamedParameterJdbcTemplate().query(sql, namedParameters,
					new BeanPropertyRowMapper(entity.getClass()));
		} catch (Exception e) {
			bitacora.error("GenericDAO.findByCriteriaForeign. Causa: " + e.getMessage() + ". Query " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
		return results;
	}

	public List findByQueryForeign(Object entity, String sql) throws AppException {
		List results = null;
		try {
			SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
			results = this.getNamedParameterJdbcTemplate().query(sql, namedParameters,
					new BeanPropertyRowMapper(entity.getClass()));
		} catch (Exception e) {
			bitacora.error("GenericDAO.findByQueryForeign. Causa: " + e.getMessage() + ". Query " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
		return results;
	}

	public List<Map<String, Object>> findByCriteria(String sql) throws AppException {
		List<Map<String, Object>> results = null;
		try {
			results = this.getNamedParameterJdbcTemplate().queryForList(sql, new HashMap<String, String>());
		} catch (Exception e) {
			bitacora.error("GenericDAO.findByCriteria. Causa: " + e.getMessage() + ". Query " + sql);
			throw bmBaseExcepcionService.throwException(Parameters.CODIGO_EXCEPCION_GENERIC, null);
		}
		return results;
	}

	public Map<String, Object> findByPKGeneral(String sql, Map<String, ?> parametros) throws AppException {
		List<Map<String, Object>> results = null;
		try {
			results = this.getNamedParameterJdbcTemplate().queryForList(sql, parametros);
		} catch (Exception e) {
			bitacora.error("GenericDAO.findByPKGeneral. Causa: " + e.getMessage() + ". Query " + sql);
			throw bmBaseExcepcionService.throwException("base01", null);
		}
		if (results != null && results.size() > 0) {
			return results.get(0);
		}
		return null;
	}

	public Connection getDataConnection() throws AppException {
		try {
			return getConnection();
		} catch (Exception e) {
			bitacora.error("GenericDAO.getConnection. Causa: " + e.getMessage());
			throw bmBaseExcepcionService.throwException("base01", null);
		}
	}

	public List executeParametizedQuery(Object entity, Long idParametro) throws Exception {
		List results = null;
		String consultaParametro = null;
		String consultaParametrizada = null;
		Map<String, Object> parametros = null;

		consultaParametro = "select valor from tliqca_parametros where id_param= :idParametro";
		parametros = new HashMap<String, Object>();
		parametros.put("idParametro", idParametro);
		consultaParametrizada = (String) (findByPKGeneral(consultaParametro, parametros)).get("valor");

		SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(entity);
		results = this.getNamedParameterJdbcTemplate().query(consultaParametrizada, namedParameters,
				new BeanPropertyRowMapper(entity.getClass()));

		return results;
	}

	public List<Map<String, Object>> findByCriteriaGeneral(String sql, Map<String, ?> parametros) throws Exception {
		return this.getNamedParameterJdbcTemplate().queryForList(sql, parametros);
	}

}
