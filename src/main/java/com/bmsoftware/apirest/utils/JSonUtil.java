package com.bmsoftware.apirest.utils;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

public class JSonUtil {

	public static String convertirObjetoAJson(Object objeto) throws Exception { // Se recibe como parametro el objeto de
																				// tipo Object por lo tanto puede tomar
																				// cualquier tipo de objeto.
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(objeto);
	}

	// Metodo para convertir una cadena json a objeto.
	public static <T> T convertirJsonAObjeto(String json, T objeto) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return (T) mapper.readValue(json, objeto.getClass());
	}

	public static JsonNode getJsonNode(String json) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readTree(json);
	}

	// Metodo para convertir una cadena json a una coleccion.
	public static <T> List<T> convertirJsonAColeccion(String json, T clase) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		CollectionType javaType = mapper.getTypeFactory()
			      .constructCollectionType(List.class, clase.getClass());
		return mapper.readValue(json, javaType);
	}

	public static Map<String, Object> getMapFromJson(String json) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, new TypeReference<Map<String, Object>>() {
		});
	}
}
