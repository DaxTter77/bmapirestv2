package com.bmsoftware.apirest.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;

import com.bmsoftware.apirest.entities.Token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * 
 * @author <a href="mailto:david.sanchez@premize.com">David Alejandro Sanchez
 *         Calle</a>
 * @project WebServiceBase
 * @class JwtUtil
 * @HU_CU_REQ
 * @date 5/03/2018
 */
public final class JwtUtil {

	private JwtUtil() {
	}

	private static final String SECRET = "20J4v35872r14n4";

	/**
	 * 
	 * @author <a href="mailto:david.sanchez@premize.com">David Alejandro
	 *         Sanchez Calle</a>
	 * @date 5/03/2018
	 * @HU_CU_REQ
	 * @param token
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Token parseToken(String token) throws ServletException {

		try {
			Token u = new Token();
			Claims body = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
			u.setUserName(body.getSubject());
			u.setFechaExpiracion((Integer) body.get("exp"));
			return u;

		} catch (ExpiredJwtException e) {
			throw new ServletException("El token ha expirado");
		} catch (JwtException | ClassCastException e) {
			throw new ServletException("Token invalido " + e.getMessage());
		}
	}

	/**
	 * @HU_CU_REQ
	 * @param user
	 * @return
	 */
	public static String generateToken(Token user) {

		return Jwts.builder().setSubject(user.getUserName()).setIssuedAt(new Date())
				.signWith(SignatureAlgorithm.HS256, SECRET).setExpiration(getExpirationDate()).compact();

	}

	/**
	 * @HU_CU_REQ
	 * @return
	 */
	private static Date getExpirationDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, 60);
		return calendar.getTime();
	}

}