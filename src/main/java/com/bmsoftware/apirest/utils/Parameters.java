package com.bmsoftware.apirest.utils;

/**
 * Clase para acceso a variables estaticas definidas.
 * 
 * @author jhon.andrey
 *
 */
public class Parameters {
	public static final Boolean ENABLE = true;
	public static final Boolean DISABLE = false;
	public static final String ACTIVO = "S";
	public static final String INACTIVO = "N";

	public static final String PREFIJO_APLICACION = "tyt_";
	public static final String ESQUEMA_TABLA_EXCEPCIONES = "";

	// NOMBRE DE LA APLICAICON CON LA SE REGISTRA EN EL LOG DE USO
	public static final String NOMBRE_APLICACION = "EJEMPLO";

	// RESOURCEBUNDLE BASE
	public static final String RESOURCE_BUNDLE_BASE = "${packageName}.i18n.labels";

	// DATOS EXCEPCION NO CONTROLADA
	public static final String CODIGO_EXCEPCION = "0";
	public static final String DESCRIPCION_EXCEPCION = "En este momento no es posible procesar su solicitud";
	public static final String SOLUCION_EXCEPCION = "Intentelo de nuevo o comuniquese con el administrador del sistema";
	// DATOS EXCEPCION CON CODIGO DE ERROR INVALIDO
	public static final String DESCRIPCION_EXCEPCION_CODIGO = "El codigo de error $1 no existe";
	public static final String SOLUCION_EXCEPCION_CODIGO = "Por favor informe esta situación al administrador del sistema";
	public static final boolean ENVIAR_NOTIFICACIONES = false;

	// MENSAJES GENERICOS
	public static final String MENSAJE_TABLA_SIN_REGISTROS = "global.mensajes.sinregistros";
	public static final String MENSAJE_REGISTRO_GUARDADO = "global.mensajes.registro.crear";
	public static final String MENSAJE_REGISTRO_MODIFICADO = "global.mensajes.registro.modificar";
	public static final String MENSAJE_REGISTRO_ELIMINADO = "global.mensajes.registro.eliminar";

	// PARAMETROS GenerarReporteBean
	// TODO JAL ACTUALIZAR VALOR PARAMETRO REAL
	public static final Integer URL_JASPER_SERVER = 25;
	public static final String PREFIJO_MIME = "application/";

	// PARAMETROS cacheService
	public static final String CACHE_PERMISOS = "Permisos";
	public static final String PATH_ARCHVIVE_CACHE = "";

	// PARAMETROS PERMISOS
	public static final int PERMISO_EJECUCION = 3;
	public static final int PERMISO_VISUALIZACION = 1;
	public static final int PERMISO_HABILITACION = 2;
	public static final int PERMISO_GET_CONSULTA = 4;

	public static final String PARAMETRO_NOMBRES_HOJAS = "";
	public static final String FORMATO_PDF = "pdf";
	public static final String FORMATO_EXCEL = "pdf";

	// EXCEPCIONES
	public static final String CODIGO_EXCEPCION_GENERIC = "base001";
	public static final String CODIGO_EXCEPCION_SERVICE = "base002";
	public static final String CODIGO_EXCEPCION_CONTROLLER = "base003";
	public static final String CODIGO_EXCEPCION_NOMBRE_REPETIDO = "base004";

	public enum TliqcaParametros {
		RUBROS(27L), CONSULTA_OBLIGACIONES(28L), USUARIO_LIQUIDACION(29L), CONSULTA_NEGOCIO_MORA(30L);

		private Long value;

		private TliqcaParametros(Long value) {
			this.value = value;
		}

		public Long getValue() {
			return value;
		}
	}

	public enum TliqcaEstadosLiquidacion {
		ACTIVA(1L), ANULADA(3L), PAGADA(2L);

		private Long value;

		private TliqcaEstadosLiquidacion(Long value) {
			this.value = value;
		}

		public Long getValue() {
			return value;
		}
	}

	public enum TliqcaCondicionesLiquidacion {
		AL_DIA(1L), EN_MORA(2L), EN_MORA_DEMANDA(3L);

		private Long value;

		private TliqcaCondicionesLiquidacion(Long value) {
			this.value = value;
		}

		public Long getValue() {
			return value;
		}
	}

	public enum TliqcaTipoPago {
		PAGO_MINIMO(2L), PAGO_ABIERTO(5L);

		private Long value;

		private TliqcaTipoPago(Long value) {
			this.value = value;
		}

		public Long getValue() {
			return value;
		}
	}
	
	public enum ExcepcionesApp {
		EXCEPCION_1("1","No se encontro el negocio asociado al identificador: $1",""),
		EXCEPCION_2("2","No estan parametrizados los rubros para el calculo de los honorarios de los abogados","Por favor parametrizar los rubros en la pantalla de rubros"),
		EXCEPCION_3("3","No estan parametrizados los honorarios para el abogado: $1","Por favor parametrizar el porcentaje de los honorarios para el abogado"),
		EXCEPCION_4("4","No esta parametrizado el producto liquidacion para el abogado: $1","Por favor parametrizar el producto liquidacion del abogado");

		private String codigo;
		private String descripcion;
		private String solucion;

		private ExcepcionesApp(String codigo, String descripcion, String solucion) {
			this.codigo = codigo;
			this.descripcion= descripcion;
			this.solucion=solucion;
		}

		public String getCodigo() {
			return codigo;
		}

		public void setCodigo(String codigo) {
			this.codigo = codigo;
		}

		public String getDescripcion() {
			return descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}

		public String getSolucion() {
			return solucion;
		}

		public void setSolucion(String solucion) {
			this.solucion = solucion;
		}
	}

}
