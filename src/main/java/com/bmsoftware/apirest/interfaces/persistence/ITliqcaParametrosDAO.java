package com.bmsoftware.apirest.interfaces.persistence;

import java.util.List;

import com.bmsoftware.apirest.entities.PersonaDTO;
import com.bmsoftware.apirest.entities.TliqcaParametrosDTO;
import com.bmsoftware.apirest.exception.AppException;

public interface ITliqcaParametrosDAO {

	/**
	 * Persistir un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a persistir
	 * @throws AppException
	 */
	public Long create(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Actiualizar un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a actualizar
	 * @throws AppException
	 */
	public void update(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Borrar un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a borrar
	 * @throws AppException
	 */
	public void delete(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Obtener un listado de objetos usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws AppException
	 */
	public List<TliqcaParametrosDTO> findByCriteria(TliqcaParametrosDTO entity, String orderField, boolean isDesc)
			throws AppException;

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws AppException
	 */
	public TliqcaParametrosDTO findByPK(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws AppException
	 */
	public List<TliqcaParametrosDTO> findByCriteriaForeign(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws AppException
	 */
	public List<TliqcaParametrosDTO> consultarNombre(TliqcaParametrosDTO entity, boolean creating) throws AppException;

	public List<PersonaDTO> findPersona(PersonaDTO entity) throws AppException;

}
