package com.bmsoftware.apirest.interfaces.services;

import java.util.List;

import com.bmsoftware.apirest.entities.PersonaDTO;
import com.bmsoftware.apirest.entities.TliqcaParametrosDTO;
import com.bmsoftware.apirest.exception.AppException;

import java.util.HashMap;

/**
 * Clase service ITliqcaParametrosService
 * 
 * @author bmsoftGenerator
 * @since V1.0
 */
public interface ITliqcaParametrosService {
	/**
	 * Persistir un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a persistir
	 * @throws Exception
	 */
	public Long create(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Actiualizar un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a actualizar
	 * @throws Exception
	 */
	public void update(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Borrar un objeto en la DB
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity object a borrar
	 * @throws Exception
	 */
	public void delete(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Obtener un listado de objetos usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public List<TliqcaParametrosDTO> findByCriteria(TliqcaParametrosDTO entity, String orderField, boolean isDesc)
			throws AppException;

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public TliqcaParametrosDTO findByPK(TliqcaParametrosDTO entity) throws AppException;

	/**
	 * Obtener una tupla usando un filtro
	 * 
	 * @author bmsoftGenerator
	 * @since V1.0
	 * @param entity filtro a usar
	 * @throws Exception
	 */
	public List<TliqcaParametrosDTO> findByCriteriaForeign(TliqcaParametrosDTO entity) throws AppException;
	
	public List<PersonaDTO> findPersona(PersonaDTO entity) throws AppException;

}