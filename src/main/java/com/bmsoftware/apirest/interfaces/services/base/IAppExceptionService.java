package com.bmsoftware.apirest.interfaces.services.base;

import com.bmsoftware.apirest.exception.AppException;

/**
 * @author wilferac
 * 
 */

public interface IAppExceptionService {

	/**
	 * Carga los datos del error desde la BD
	 * 
	 * @param codigo codigo del error
	 * @param dato   datos para el reemplazo de los comodines EJ: para el error
	 *               "Ocurrio un error con el campo $1, verifique que tenga el
	 *               formato $2" enviariamos un array {"email", "email@dominio"} el
	 *               resultado seria "Ocurrio un error con el campo email, verifique
	 *               que tenga el formato email@dominio" "
	 * @return
	 * @throws Exception
	 */
	public AppException throwException(String codigo, String[] dato);

}
