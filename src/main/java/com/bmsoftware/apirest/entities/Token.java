package com.bmsoftware.apirest.entities;

import java.util.List;

public class Token {

	private String userName;
	private String password;
	private List<String> rol;
	private Integer fechaExpiracion;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<String> getRol() {
		return rol;
	}

	public void setRol(List<String> rol) {
		this.rol = rol;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getFechaExpiracion() {
		return fechaExpiracion;
	}

	public void setFechaExpiracion(Integer fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}

}
