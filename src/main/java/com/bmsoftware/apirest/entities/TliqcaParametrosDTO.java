package com.bmsoftware.apirest.entities;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;
import java.util.HashMap;

public class TliqcaParametrosDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	// objeto que contendra el aleas de las respectivas foraneas
	private HashMap<String, String> aliasForaneas;

	private Long idParam;
	private String nombre;
	private String descripcion;
	private String valor;

	// Objeto que representan las relaciones de la tabla

	public TliqcaParametrosDTO() {

		// se llena el objeto con los alias de la tabla
		aliasForaneas = new HashMap<>();
	}

	public HashMap<String, String> getAliasForaneas() {
		return aliasForaneas;
	}

//Metodos Get y Set de las variables de la clase

	public void setIdParam(Long idParam) {
		this.idParam = idParam;
	}

	public Long getIdParam() {
		return idParam;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}

//metodos get  de los objeto foraneos 

//metodos Get y Set de los atributos de los objetos foraneos      

}