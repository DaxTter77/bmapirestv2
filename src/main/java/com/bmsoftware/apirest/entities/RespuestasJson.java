package com.bmsoftware.apirest.entities;

import java.io.Serializable;
import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class RespuestasJson implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String mensaje;
	private Collection resultado;
	
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Collection getResultado() {
		return resultado;
	}
	public void setResultado(Collection resultado) {
		this.resultado = resultado;
	}
	
	
}
