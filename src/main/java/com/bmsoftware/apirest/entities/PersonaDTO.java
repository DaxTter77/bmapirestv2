package com.bmsoftware.apirest.entities;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;
import java.util.HashMap;


public class PersonaDTO implements Serializable{

	private static final long serialVersionUID = 1L;
    //objeto que contendra el aleas de las respectivas foraneas
  	private HashMap<String, String> aliasForaneas;
	
	private Long idPersona;
	private String nombre;
	private String edad;
//	private String apellido;
//	private Date fechaCreacion;
//	private String usuarioCrea;
//	private String activo;

    //Objeto que representan las relaciones de la tabla
  
  public PersonaDTO(){
	   	
	//se llena el objeto con los alias de la tabla 
	aliasForaneas=new HashMap<>();			
    	}

  public HashMap<String, String> getAliasForaneas() {
		return aliasForaneas;
	}
  

//Metodos Get y Set de las variables de la clase

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	
	public Long getIdPersona() {
		return idPersona;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
//	public void setApellido(String apellido) {
//		this.apellido = apellido;
//	}
//	
//	public String getApellido() {
//		return apellido;
//	}
//	public void setFechaCreacion(Date fechaCreacion) {
//		this.fechaCreacion = fechaCreacion;
//	}
//	
//	public Date getFechaCreacion() {
//		return fechaCreacion;
//	}
//	public void setUsuarioCrea(String usuarioCrea) {
//		this.usuarioCrea = usuarioCrea;
//	}
//	
//	public String getUsuarioCrea() {
//		return usuarioCrea;
//	}
//	public void setActivo(String activo) {
//		this.activo = activo;
//	}
//	
//	public String getActivo() {
//		return activo;
//	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}
	


//metodos get  de los objeto foraneos 
 
//metodos Get y Set de los atributos de los objetos foraneos      

     
  


}