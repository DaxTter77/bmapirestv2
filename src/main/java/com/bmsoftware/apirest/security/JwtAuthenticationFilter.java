package com.bmsoftware.apirest.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.bmsoftware.apirest.entities.Token;
import com.bmsoftware.apirest.exception.AppException;
import com.bmsoftware.apirest.interfaces.services.base.IAppExceptionService;
import com.bmsoftware.apirest.utils.JwtUtil;



/**
 * 
 * @project WebServiceBase
 * @class JwtAuthenticationFilter
 * @HU_CU_REQ
 */
@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	public String BEARER = "Bearer ";
	private static final Logger bitacora = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

	@Autowired
	private IAppExceptionService appExceptionService;
	
//	@Autowired
//	private IRolService rolService;

	/**
	 * 
	 * 
	 * @param request
	 * @param response
	 * @param filterChain
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		String authHeader = request.getHeader("authorization");
		if (authHeader != null && authHeader.startsWith(BEARER)) {
			SecurityContextHolder.getContext().setAuthentication(null);
			String url = request.getRequestURI().substring(request.getContextPath().length());
			String[] paths = url.split("/");
			url = "/" + paths[1];
			System.out.println(url);

			String token = authHeader.substring(BEARER.length());
			try {
				Token datosToken = JwtUtil.parseToken(token);
				//Manejo de los datos del token y dar acceso a los roles
				//Ejemplo que se usaba en salesforce
//				List<String> listaEndpoints = rolService.getEndpointByRol(datosToken.getRol());
//				if (tienePermisoUrl(listaEndpoints, url)) {
//					asignarPermisos(datosToken.getRol());
//				} else {
//					throw appExceptionService.throwException(Parameters.CODIGO_EXCEPCION_JWT, null);
//				}

				filterChain.doFilter(request, response);

//			}catch(AppException e) {
//				throw new ServletException(e.getDescripcion()+"."+e.getSolucion());
			} catch (Exception e) {
				bitacora.error("JwtAuthenticationFilter.doFilterInternal. Causa: " + e.getMessage(), e);
				throw new ServletException(e.getMessage());
			}
		} else {
			filterChain.doFilter(request, response);
		}
	}

	/**
	 * 
	 * @author <a href="mailto:juandavidtvs@gmail.com">David Alejandro Sanchez
	 *         Calle</a>
	 * @date 17/09/2019
	 * @HU_CU_REQ
	 * @param listaEndpoints
	 * @param url
	 * @return
	 * @throws ServletException
	 */
	private boolean tienePermisoUrl(List<String> listaEndpoints, String url) throws ServletException {
		boolean permitido = false;
		for (String endpoint : listaEndpoints) {
			if (url.equals(endpoint)) {
				permitido = true;
				break;
			}
		}
		return permitido;
	}

	/**
	 * 
	 * @HU_CU_REQ
	 * @param roles
	 */
	private void asignarPermisos(List<String> roles) {
		ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
		for (String rol : roles) {
			grantedAuthorities.add(new SimpleGrantedAuthority(rol));
		}
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken("", null,
				grantedAuthorities);
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(auth);
	}

}
