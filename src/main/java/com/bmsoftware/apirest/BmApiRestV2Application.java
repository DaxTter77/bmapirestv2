package com.bmsoftware.apirest;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class BmApiRestV2Application {
	
	@Autowired
	Environment env;
	
//	@Autowired
//	JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	
//	@Autowired
//	JwtAuthenticationFilter jwtAuthenticationFilter;

	public static void main(String[] args) {
		SpringApplication.run(BmApiRestV2Application.class, args);
	}

	@Configuration
	@EnableWebSecurity
	public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		public void configure(WebSecurity web) {
			web.ignoring().anyRequest();
		}
		
		//En este metodo se define los filtros de seguridad para la autenticación
//		@Override
//		protected void configure(HttpSecurity http) throws Exception {
//			String[] resources = new String[]{
//		            "/include/**","/faces/javax.faces.resource/**","/faces/","/css/**","/icons/**","/img/**","/js/**","/layer/**","/configuration/ui",
//		            "/swagger-resources/*","/swagger-ui*","/swagger-ui.html/*","/v2/api-docs","/configuration/ui","/swagger-resources/**","/configuration/security",
//		            "/webjars/**"
//		    };
//			
//			http.csrf().disable()
//				.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
//				.and()
//				.addFilterAfter(jwtAuthenticationFilter, CustomUsernamePasswordAuthenticationFilter.class)
//				.authorizeRequests()
//				.antMatchers(resources).permitAll()
//				.anyRequest().authenticated();
//			
//		}


		@Bean(name = "principalDataSource")
		@Primary
		@ConfigurationProperties(prefix = "spring.datasource")
		public DataSource masterDataSource() {
			DriverManagerDataSource dataSource = new DriverManagerDataSource();
			dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
			dataSource.setUrl(env.getProperty("spring.datasource.url"));
			dataSource.setUsername(env.getProperty("spring.datasource.username"));
			dataSource.setPassword(env.getProperty("spring.datasource.password"));
			return dataSource;
		}
	}
}
