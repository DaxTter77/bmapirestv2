package com.bmsoftware.apirest.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.bmsoftware.apirest.entities.PersonaDTO;
import com.bmsoftware.apirest.entities.RespuestasJson;
import com.bmsoftware.apirest.entities.TliqcaParametrosDTO;
import com.bmsoftware.apirest.interfaces.services.ITliqcaParametrosService;
import com.bmsoftware.apirest.utils.Parameters;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ExampleController {

	@Autowired
	private ITliqcaParametrosService parametrosService;
	

	@GetMapping("/example")
	public ResponseEntity<?> findAll() {
//		TliqcaParametrosDTO parametro = null;
		PersonaDTO parametro = null;
		RespuestasJson respuesta = null;
		try {
//			parametro = new TliqcaParametrosDTO();
//			parametro.setIdParam(Parameters.TliqcaParametros.RUBROS.getValue());
//			parametro = parametrosService.findByPK(parametro);
			List<PersonaDTO> list = parametrosService.findPersona(new PersonaDTO());
//			if (parametro != null && parametro.getValor() != null) {
			if(list != null) {
				respuesta = new RespuestasJson();
//				List<TliqcaParametrosDTO> listaRes = new ArrayList<TliqcaParametrosDTO>();
				List<PersonaDTO> listaRes = new ArrayList<>();
				
				respuesta.setMensaje("Resultado exitoso");
				respuesta.setResultado(list);
				
				ObjectMapper objectMapper = new ObjectMapper();
				String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(respuesta);
				return ResponseEntity.ok(json);
			}
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/example/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id) {
//		TliqcaParametrosDTO parametro = null;
		PersonaDTO parametro = null;
		RespuestasJson respuesta = null;
		try {
//			parametro = new TliqcaParametrosDTO();
//			parametro.setIdParam(Parameters.TliqcaParametros.RUBROS.getValue());
//			parametro = parametrosService.findByPK(parametro);
			
			parametro = new PersonaDTO();
//			if (parametro != null && parametro.getValor() != null) {
			if(parametro != null) {
				respuesta = new RespuestasJson();
//				List<TliqcaParametrosDTO> listaRes = new ArrayList<TliqcaParametrosDTO>();
				List<PersonaDTO> listaRes = new ArrayList<>();
				
				parametro.setNombre("Roberto id" + id.intValue());
				listaRes.add(parametro);
				respuesta.setMensaje("Consultado Correctamente");
				respuesta.setResultado(listaRes);
				
				ObjectMapper objectMapper = new ObjectMapper();
				String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(respuesta);
				return ResponseEntity.ok(json);
			}
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		return ResponseEntity.notFound().build();
	}
	
	
	@PostMapping("/example")
	public ResponseEntity<?> create(@RequestBody TliqcaParametrosDTO body) {
		RespuestasJson respuesta = null;
		try {
			
//			Long id = parametrosService.create(body);
			Long id = body.getIdParam();
			if (id != null && id.intValue() > 0) {
				respuesta = new RespuestasJson();
				
				respuesta.setMensaje("Creado correctamente");
				ObjectMapper objectMapper = new ObjectMapper();
				String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(respuesta);
				return ResponseEntity.status(HttpStatus.CREATED).body(json);
			}else {
				throw new Exception("El parametro está vacío");
			}
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	@PutMapping("/example")
	public ResponseEntity<?> update(@RequestBody TliqcaParametrosDTO body) {
		RespuestasJson respuesta = null;
		try {
			
//			parametrosService.update(body);
			if(body.getIdParam() == null || body.getIdParam() == 0) {
				throw new Exception("No llegó el id del parametro");
			}
			respuesta = new RespuestasJson();
			respuesta.setMensaje("Modificado correctamente");
			ObjectMapper objectMapper = new ObjectMapper();
			String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(respuesta);

			return ResponseEntity.ok(json);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
	
	@DeleteMapping("/example/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		TliqcaParametrosDTO entity = null;
		RespuestasJson respuesta = null;
		try {
			entity = new TliqcaParametrosDTO();
			entity.setIdParam(id);
			
//			parametrosService.delete(entity);
			respuesta = new RespuestasJson();
			
			respuesta.setMensaje("Eliminado correctamente");
			ObjectMapper objectMapper = new ObjectMapper();
			String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(respuesta);

			return ResponseEntity.ok(json);
		} catch (Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}
}
