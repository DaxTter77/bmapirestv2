package com.bmsoftware.apirest.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bmsoftware.apirest.exception.AppException;
import com.bmsoftware.apirest.interfaces.persistence.IGenericDAO;
import com.bmsoftware.apirest.interfaces.services.base.IAppExceptionService;
import com.bmsoftware.apirest.persistence.GenericDAO;
import com.bmsoftware.apirest.utils.Parameters;

/**
 * @author wilferac
 * 
 */
@Service("BMBaseExceptionService")
public class AppExceptionService extends GenericDAO implements IAppExceptionService {

	private static final Logger bitacora = Logger.getRootLogger();

	@Override
	public AppException throwException(String codigo, String[] dato) {
		AppException exc = new AppException(codigo, dato);
		try {
			//Si son de las excepciones base damos los valores localmente
			if(codigo.equalsIgnoreCase(Parameters.CODIGO_EXCEPCION_GENERIC)) {
				exc.setCodigo(codigo);
				exc.setDescripcion("En este momento no es posible atender su solicitud");
				exc.setSolucion("Por favor informe al administrado del sistema indicando el codio de error "+Parameters.CODIGO_EXCEPCION_GENERIC);
			}
			else if(codigo.equalsIgnoreCase(Parameters.CODIGO_EXCEPCION_SERVICE)) {
				exc.setCodigo(codigo);
				exc.setDescripcion("En este momento no es posible atender su solicitud");
				exc.setSolucion("Por favor informe al administrado del sistema indicando el codio de error "+Parameters.CODIGO_EXCEPCION_SERVICE);
			}
			else if(codigo.equalsIgnoreCase(Parameters.CODIGO_EXCEPCION_CONTROLLER)) {
				exc.setCodigo(codigo);
				exc.setDescripcion("En este momento no es posible atender su solicitud");
				exc.setSolucion("Por favor informe al administrado del sistema indicando el codio de error "+Parameters.CODIGO_EXCEPCION_CONTROLLER);
			}
			else if(codigo.equalsIgnoreCase(Parameters.CODIGO_EXCEPCION_NOMBRE_REPETIDO)) {
				exc.setCodigo(codigo);
				exc.setDescripcion("El nombre del registro ya existe");
				exc.setSolucion("Debe ingresar un nombre de registro diferente");
			}
			else {
				String query = "select codigo, descripcion, solucion, soporte from excepciones_app where codigo = :codigo";
				exc = (AppException) super.findByPK(exc, query);
			}
			// Si no se encuentra la excepcion el codigo no existe
			if (exc == null) {
				exc = new AppException();
				exc.setDescripcion(Parameters.DESCRIPCION_EXCEPCION_CODIGO);
				exc.setSolucion(Parameters.SOLUCION_EXCEPCION_CODIGO);
				String[] datos = new String[1];
				datos[0] = codigo;
				exc.setDato(datos);
				exc.loadData();
				return exc;
			}
			exc.setDato(dato);
			exc.loadData();
		} catch (Exception e) {
			bitacora.error("JaverianaException.throwException. Causa: " + e.getMessage() + ".");
			exc.setDescripcion(Parameters.DESCRIPCION_EXCEPCION);
			exc.setSolucion(Parameters.SOLUCION_EXCEPCION);
		}
		return exc;
	}

}
