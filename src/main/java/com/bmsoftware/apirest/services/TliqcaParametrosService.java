package com.bmsoftware.apirest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Date;
import java.util.List;

import com.bmsoftware.apirest.entities.PersonaDTO;
import com.bmsoftware.apirest.entities.TliqcaParametrosDTO;
import com.bmsoftware.apirest.exception.AppException;
import com.bmsoftware.apirest.interfaces.persistence.ITliqcaParametrosDAO;
import com.bmsoftware.apirest.interfaces.services.ITliqcaParametrosService;
import com.bmsoftware.apirest.interfaces.services.base.IAppExceptionService;
import com.bmsoftware.apirest.utils.Parameters;

@Service("TliqcaParametrosService")
public class TliqcaParametrosService implements ITliqcaParametrosService, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger bitacora = Logger.getRootLogger();

	@Autowired
	private ITliqcaParametrosDAO accessData;

	@Autowired
	private IAppExceptionService exceptionService;

	@Override
	@Transactional(rollbackFor = AppException.class)
	public Long create(TliqcaParametrosDTO entity) throws AppException {
		try {
			validateName(entity, true);
			return accessData.create(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosService.create. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_SERVICE, null);

		}
	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void update(TliqcaParametrosDTO entity) throws AppException {
		try {
			validateName(entity, false);
			accessData.update(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosService.update. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_SERVICE, null);

		}

	}

	@Override
	@Transactional(rollbackFor = AppException.class)
	public void delete(TliqcaParametrosDTO entity) throws AppException {
		try {
			accessData.delete(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosService.delete. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_SERVICE, null);

		}

	}

	@Override
	public List<TliqcaParametrosDTO> findByCriteria(TliqcaParametrosDTO entity, String orderField, boolean isDesc)
			throws AppException {
		try {
			return accessData.findByCriteria(entity, orderField, isDesc);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosService.findByCriteria. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_SERVICE, null);

		}

	}

	@Override
	public TliqcaParametrosDTO findByPK(TliqcaParametrosDTO entity) throws AppException {
		try {
			return accessData.findByPK(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosService.findByPK. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_SERVICE, null);

		}

	}

	@Override
	public List<TliqcaParametrosDTO> findByCriteriaForeign(TliqcaParametrosDTO entity) throws AppException {
		try {
			return accessData.findByCriteriaForeign(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosService.findByCriteriaForeign. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_SERVICE, null);

		}
	}

	private void validateName(TliqcaParametrosDTO entity, boolean creating) throws AppException {
		try {
			List<TliqcaParametrosDTO> list = accessData.consultarNombre(entity, creating);
			if (list != null && list.size() > 0)
				throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_NOMBRE_REPETIDO, null);

		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosService.validateName. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_SERVICE, null);

		}
	}

	@Override
	public List<PersonaDTO> findPersona(PersonaDTO entity) throws AppException {
		try {
			return accessData.findPersona(entity);
		} catch (AppException je) {
			throw je;
		} catch (Exception e) {
			bitacora.error("TliqcaParametrosService.findByCriteriaForeign. Causa: " + e.getMessage(), e);
			throw exceptionService.throwException(Parameters.CODIGO_EXCEPCION_SERVICE, null);

		}
	}

}
